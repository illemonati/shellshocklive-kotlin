package tech.tioft.shellshocklive

import javafx.application.Application
import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.TextInputDialog
import javafx.scene.input.KeyCode
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.stage.Screen
import javafx.stage.Stage
import javafx.stage.StageStyle
import org.jnativehook.GlobalScreen
import org.jnativehook.keyboard.NativeKeyEvent

import org.jnativehook.keyboard.NativeKeyListener
import org.jnativehook.mouse.NativeMouseEvent
import org.jnativehook.mouse.NativeMouseListener
import java.lang.IllegalArgumentException
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.system.exitProcess


fun main(args: Array<String>) {
    Application.launch(GuiMain::class.java, *args)
}


class Listener(private val primaryStage: Stage, private val mainGui: GuiMain): NativeKeyListener, NativeMouseListener {
    override fun nativeKeyTyped(nke: NativeKeyEvent?) {
    }

    override fun nativeKeyPressed(p0: NativeKeyEvent?) {
        if (p0 != null) {
            when(p0.keyCode) {
                NativeKeyEvent.VC_Q -> Platform.runLater {
//                    primaryStage.isIconified = !primaryStage.isIconified
                    if (mainGui.isHidden) {
                        mainGui.isHidden = false
                        primaryStage.show()
                    } else {
                        mainGui.isHidden = true
                        primaryStage.hide()
                        autoFocusShellShock()
                    }
                }
                NativeKeyEvent.VC_ESCAPE -> Platform.runLater { primaryStage.close(); exitProcess(0) }
                NativeKeyEvent.VC_S -> Platform.runLater {mainGui.currentSetting = Setting.SELF}
                NativeKeyEvent.VC_O -> Platform.runLater {mainGui.currentSetting = Setting.OPPONENT}
                NativeKeyEvent.VC_R -> Platform.runLater {mainGui.currentSetting = Setting.RADIUS}
                NativeKeyEvent.VC_W -> Platform.runLater {mainGui.handleWindInput(primaryStage)}
                else -> Platform.runLater {mainGui.currentSetting = Setting.None}
            }
        }
    }

    override fun nativeKeyReleased(p0: NativeKeyEvent?) {
    }

    private fun calcRealXY(x: Int, y: Int) : Pair<Int, Int> =
        Pair(
            (x - primaryStage.scene.x).toInt(),
            (y - primaryStage.scene.y).toInt()
        )

    private fun autoFocusShellShock() {
        when(System.getProperty("os.name")) {
            "Mac OS X" -> {
                val process = Runtime.getRuntime().exec(arrayOf("osascript", "-e", "tell app \"ShellShockLive\" to activate"))
            }
        }
    }

    override fun nativeMousePressed(p0: NativeMouseEvent) {
        when(mainGui.currentSetting) {
            Setting.SELF -> mainGui.selfPos = calcRealXY(p0.x, p0.y)
            Setting.RADIUS -> {
                val (x, y) = calcRealXY(p0.x, p0.y)
                mainGui.changeRadius(x, y)
            }
            Setting.OPPONENT -> mainGui.opponentPos = calcRealXY(p0.x, p0.y)
            else -> {}
        }
        if (mainGui.currentSetting != Setting.Wind) {
            mainGui.currentSetting = Setting.None
        }
    }

    override fun nativeMouseClicked(p0: NativeMouseEvent?) {
    }

    override fun nativeMouseReleased(p0: NativeMouseEvent?) {
    }
}

enum class Setting {
    SELF, OPPONENT, RADIUS, Wind, None
}

class GuiMain : Application() {

    private lateinit var primaryStage: Stage
    private lateinit var pane: Pane

    private var infoText = Text()
    private var selfCircle = Circle()
    private var opponentCircle = Circle()
    var selfPos = Pair(-1, -1)
        set(value) {
            field = value
            selfCircle.centerX = value.first.toDouble()
            selfCircle.centerY = value.second.toDouble()
        }
    var opponentPos = Pair(-1, -1)
        set(value) {
            field = value
            opponentCircle.centerX = value.first.toDouble()
            opponentCircle.centerY = value.second.toDouble()
        }

    var wind = 0

    var radius = 0.0

    var angle = 0.0

    var distance = 0.0

    var isHidden = false

    var currentSetting = Setting.None
        set(value) {
            field = value
            calcDistance()
            calcAngle()
            drawInfo()
        }

    private fun calcDistance() {
        distance = sqrt((selfPos.first - opponentPos.first).toDouble().pow(2) + (selfPos.second - opponentPos.second).toDouble().pow(2))
    }

    private fun calcAngle() {
        angle = (90.0 - 4 * (distance/radius) + wind / 14.0)
    }


    fun changeRadius(x: Int, y: Int) {
        val sx = selfPos.first
        val sy = selfPos.second
        if (sx < 0 || sy < 0) {
            return
        }
        val d = sqrt(((x-sx).toDouble().pow(2) + (y-sy).toDouble().pow(2)))
        radius = d
    }




    override fun start(primaryStage: Stage) {
        val logger: Logger = Logger.getLogger(GlobalScreen::class.java.getPackage().name)
        logger.level = Level.WARNING
        logger.useParentHandlers = false;
        this.primaryStage = primaryStage
        GlobalScreen.registerNativeHook()
        GlobalScreen.addNativeKeyListener(Listener(primaryStage, this))
        GlobalScreen.addNativeMouseListener(Listener(primaryStage, this))
        Platform.setImplicitExit(false);


        val root = Pane()
        pane = root
        val scene = Scene(root, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Color.TRANSPARENT)
        root.setMinSize(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY)
        root.style = "-fx-background-color: transparent;";
        scene.fill = Color.TRANSPARENT
        primaryStage.initStyle(StageStyle.TRANSPARENT)
        primaryStage.title = "Shell Shock Live Aim"
        primaryStage.scene = scene
        primaryStage.isAlwaysOnTop = true
        primaryStage.width = Screen.getPrimary().bounds.width;
        primaryStage.height = Screen.getPrimary().bounds.height;
//        primaryStage.isMaximized = true

        infoText.font = Font(20.0)
        infoText.fill = Color.AQUAMARINE
        infoText.relocate(100.0, 100.0)

        selfCircle.radius = 10.0
        selfCircle.fill = Color.GREEN

        opponentCircle.radius = 10.0
        opponentCircle.fill = Color.RED

        pane.children.addAll(infoText, selfCircle, opponentCircle)

        drawInfo()




        primaryStage.show()
    }

    fun handleWindInput(stage: Stage) {
        currentSetting = Setting.Wind
        val windInputDialog = TextInputDialog(wind.toString())
        windInputDialog.headerText = "Input new wind"
        windInputDialog.initOwner(stage)
        windInputDialog.showAndWait()
        val input = windInputDialog.editor.text
        try {
            wind = input.toInt()
        } catch (e: IllegalArgumentException) {
            val alert = Alert(Alert.AlertType.ERROR, "That was not a valid integer!")
            alert.initOwner(stage)
            alert.showAndWait()
        }
        currentSetting = Setting.None
    }


    private fun drawInfo() {
        drawText()
    }

    private fun drawText() {
        infoText.text = """
                       >Self Pos : ${selfPos.toString()}
                       >Opponent Pos : ${opponentPos.toString()}
                       >Distance : ${distance.toString()}
                       >Wind : ${wind.toString()}
                       >Radius : ${radius.toString()}
                       >Angle : ${angle.toString()}
                       >Input Setting : ${currentSetting.toString()}
                       """.trimMargin(">")
    }


}


